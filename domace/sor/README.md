SOR iteracija za razpršene matrike
===========================

Navodila
----------

Naj bo $`A`$ $`n \times n`$ diagonalno dominantna razpršena matrika(velika večina elementov je ničelnih $`a_{ij} = 0`$). Matriko zaradi prostorskih zahtev hranimo v dveh matrikah. Naj bosta $`V`$ in $`I`$ $`n \times m`$ matriki, tako da velja
```math
V(i,j) = A(i,I(i,j)).
```
V matriki $`V`$ se torej nahajajo neničelni elementi matrike $`A`$. Vsaka vrstica matrike $`V`$ vsebuje neničelne elemente iz iste vrstice v $`A`$. V matriki $`I`$ pa so shranjeni indeksi stolpcev teh neničelnih elementov.

Napišite funkcijo $`[x,i] = sor(V,I,b,omega)`$, ki reši sistem
```math
Ax = b
```
z metodo SOR za razpršeno matriko $`A`$ predstavljeno z matrikami $`V`$ in $`I`$.

Napišite še funkcijo $`b = zmnozi(V,I,x)`$, ki pomnoži vektor $`b`$ z razpršeno matriko. Za testne primere poiščite optimalni $`omega`$, pri katerem SOR najhitreje konvergira.


Opis rešitve
----------

Rešitev vsebuje zgoraj opisani funkciji $`sor`$ in $`zmnozi`$. Vsaka datoteka vsebuje tudi tri teste. Za večje razpršene matrike s približno enakim številom elementov v vsaki vrstici je metoda učinkovita, saj potrebuje manj prostora kot metoda za goste matrike.

Za primer klica in izvajanja pokličite $demo sor$ oziroma $demo zmnozi$.
