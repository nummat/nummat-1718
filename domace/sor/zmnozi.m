function b = zmnozi(V, I, x)
  ## Funkcija zmnozi vektor x z redko matriko V
  ## V -> matrika velikosti n x m
  ## I -> matrika velikosti n x m
  ## x -> vrsticni vektor za mnozenje z matriko
  ## b -> izhodni zmnozeni vektor
  
  ## ce je x slucajno podan kot stolpicni vektor ga transponiraj
  [h w] = size(x);
  if w==1
    x = x';
  endif
  
  ## pridobi vrednosti na pozicijah
  ## vals je matrika z pripadajocimi 
  ## vrednostmi na ustreznih pozicijah
  vals = x(I);
  
  ## zmnozi pridobene vrednosti z vrednostmi matrike V 
  ## in nato sesteje po vrsticah
  b = sum(V .* vals, 2);
  
endfunction

%!demo
%! ## Demo množenja razpršene matrike A z vektorjem x
%! ## kjer je matrika A predstavljena z matrikama vrednosti V in pozicij I
%! ## Razpršena diagonalna matrika A
%! ## | 5    2    0    0    4    0    0 |
%! ## | 0    1    0    12   0    0    9 |
%! ## | 0.1  0    1.2  0    0    1    0 |
%! ## | 0    0    0    0.5  0    0    0 |
%! ## | 0    0    11   0    11   0    0 |
%! ## | 0    4    2.5  0    0    7    0 |
%! ## | 0    0    2    0    0    0    6 |
%! ##
%! ## predstavljena z matrikama
%! ##        V              I
%! ## | 5    2    4 |  | 1  2  5 |
%! ## | 1    12   9 |  | 2  4  7 |
%! ## | 0.1  1.2  1 |  | 1  3  6 |
%! ## | 0.5  0    0 |  | 4  1  2 |
%! ## | 11   11   0 |  | 3  5  1 |
%! ## | 4    2.5  7 |  | 2  3  6 |
%! ## | 2    6    0 |  | 3  7  1 |
%! ##
%! ## Vektor x
%! ## | 1  2  3  4  5  6  7 |
%!
%!  
%! V = [5 2 4; 1 12 9; 0.1 1.2 1; 0.5 0 0; 11 11 0; 4 2.5 7; 2 6 0];
%! I = [1 2 5; 2 4 7; 1 3 6; 4 1 2; 3 5 1; 2 3 6; 3 7 1];
%! A = [5    2  0    0    4   0  0; 
%!      0    1  0    12   0   0  9; 
%!      0.1  0  1.2  0    0   1  0; 
%!      0    0  0    0.5  0   0  0; 
%!      0    0  11   0    11  0  0; 
%!      0    4  2.5  0    0   7  0; 
%!      0    0  2    0    0   0  6];
%! x = 1:7;
%! b_mul_sparse = zmnozi(V, I, x)
%! b_mul_dense  = A * x'


%!test
%! V = [2  4; 
%!    -5 -10; 
%!     7  -7; 
%!    -2  -6; 
%!     5   1; 
%!     9   1;
%!     9  27;
%!     1   1;
%!     3   7;
%!    -7   8;
%!    -9   2; 
%!     1   1;
%!     0  95;
%!   -90  80];
%!
%! I = [9  1; 
%!     8   2; 
%!     3   5; 
%!    11   4; 
%!     5  10; 
%!     6  10;
%!     9   7;
%!    12   8;
%!     3   9;
%!     7  10;
%!    11   2; 
%!    12   6;
%!     2  13;
%!    14  12];
%! 
%! b = [2809 115 35 838 25 352 31.5 13 27.5 73 750 50 9.5 978]';
%! x = [702 -12 8 -111 3 38 1 1 0.5 10 -86 12 0.1 -0.2]';
%! assert(zmnozi(V, I, x), b, 0.5e-10)

%!test
%! V = [39 10   4  7; 
%!     -5 -10  20  2; 
%!      7  21 -11  1; 
%!     -2  -6   0 10; 
%!      5   1   1  1; 
%!     -11  1  -7 -1;
%!      9  68 -92  2;
%!      1  -4   8  1;
%!      1   5   1  1;
%!      2   1   2  8];
%!
%! I = [1  2  6  9;
%!      4  3  2  8;
%!      1  3  7  4;
%!      1  3  6  4;
%!      5  6  7  9;
%!      6  3  1  7;
%!      5  8  7  1;
%!      6  4  8  2;
%!      2  9  1  5;
%!      3  5  2 10];
%! 
%! b = [195.5 219 27.5 42 65.75 -25.75 200 10.25 43 95]';
%! x = [1 12.5 1 5 12 1.75 0.5 2 3.5 7]';
%! assert(zmnozi(V, I, x), b, 0.5e-10)

%!test
%! V = [100  0; 
%!      -10  1; 
%!       -2  2; 
%!       -6  3; 
%!        1 -1; 
%!     1087 29;
%!       57 -1.1;
%!        1 -6;
%!        9 70;
%!     -700 21];
%!
%! I = [ 1  5;
%!       2  6;
%!       3  1;
%!       4  2;
%!       5  3;
%!       6  4;
%!       7  9;
%!       8 10;
%!       9  8;
%!      10  7];
%! 
%! b = [200 127 2.4 30 1.2 7290 -6.4 8.1 246 702.1]';
%! x = [2 -12 0.8 -11 2 7 0.1 2.1 11 -1]';
%! assert(zmnozi(V, I, x), b, 0.5e-10)