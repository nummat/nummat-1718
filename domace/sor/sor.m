function [x,i] = sor(V, I, b, omega)
  ## Funkcija izvaja Successive over-relaxation nad diagonalno-dominantno razprseno matriko
  ## V -> matrika velikosti n x m
  ## I -> matrika velikosti n x m
  ## b -> vektor b sistema  Ax = b
  ## omega -> parameter med 0 < omega < 2. Doloca hitrost konvergence, vrednost je odvisna od strukture matrike  
  ## x -> izhodni vektor
  ## i -> izhodno stevilo iteracij
  
  [n, m] = size(V);             # stevilo elementov v posamezni dimenziji
  
  tol = 1e-11;                  # toleranca
  
  ## stevilo iteracij je doloceno glede na parameter omega
  ## odgovarja
  maxit = n*100;                # maksimalno stevilo iteracij
  
  x0 = x = zeros(n,1);          # zacetni priblizek, nastavljen na vektor nicel
  
  ## pridobi pozicije diag. elementov iz matrike I
  copyI = I;
  copyI( (I - (1:n)') != 0 ) = 0;
  copyI(copyI != 0) = 1;
  
  diagA = V'(logical(copyI'));  # vektor vrednosti diagonalnih elementov
  
  ## zanka poteka do max. stevila iteracij
  for i = 1:maxit
    
    ## SOR iteracija
    for k=1:n
      
      dlt = V(k,:) * x(I(k,:));
      dlt -= diagA(k) * x(k);
      
      x(k) = x(k) + omega*( (b(k) - dlt)/diagA(k) - x(k));
      
    endfor
  
    ## ce je najvecja napaka manjsa od meje tolerance
    if max(x-x0) < tol
      break
    endif
    
    x0 = x;
    
  endfor
  
endfunction

%!demo
%! ## Demo reševanja sistema Ax = b
%! ## za diagonalno dominantne razpršene matrike 
%! ## predstavljene z matrikama vrednosti V in pozicij I
%! ##
%! ## Razpršena diagonalno dominantna matrika A
%! ## | 5    0    0    0    0     3    0  |
%! ## | 0    0.1  0    0    0     0    0  |
%! ## | 0    0    21   0    11    0    0  |
%! ## | 0    0.2  0    96   0     0    0  |
%! ## | 0    0    2    0    11.7  0    0  |
%! ## | 3.1  0    0    0    0     14   0  |
%! ## | 0    0    0    4    0     0    9.9|
%! ##
%! ## Vektor b
%! ## | 2  10  1  9.5  1  2  4 |
%!
%!  
%! V = [5    3;
%!      0.1  0;   
%!      21   11;
%!      0.2  96;
%!      2    11.7;
%!      3.1  14;
%!      4    9.9];
%! 
%! I = [1  6;
%!      2  1;
%!      3  5;
%!      2  4;
%!      3  5;
%!      1  6;
%!      4  7];
%! b = [2 10 1 9.5 1 2 4];
%! omega = 1;
%! [x,i] = sor(V, I, b, omega)
%! b_result = zmnozi(V, I, x)


%!test
%! V = [2  4; 
%!    -5 -10; 
%!     7  -7; 
%!    -2  -6; 
%!     5   1; 
%!     9   1;
%!     9  27;
%!     1   1;
%!     3   7;
%!    -7   8;
%!    -9   2; 
%!     1   1;
%!     0  95;
%!   -90  80];
%!
%! I = [9  1; 
%!     8   2; 
%!     3   5; 
%!    11   4; 
%!     5  10; 
%!     6  10;
%!     9   7;
%!    12   8;
%!     3   9;
%!     7  10;
%!    11   2; 
%!    12   6;
%!     2  13;
%!    14  12];
%! 
%! b = [2809 115 35 838 25 352 31.5 13 27.5 73 750 50 9.5 978]';
%! omega = 1.01;
%! solution = [702 -12 8 -111 3 38 1 1 0.5 10 -86 12 0.1 -0.2]';
%! [x,i] = sor(V, I, b, omega);
%! assert(x, solution, 0.5e-10)

%!test
%! V = [39 10   4  7; 
%!     -5 -10  20  2; 
%!      7  21 -11  1; 
%!     -2  -6   0 10; 
%!      5   1   1  1; 
%!     -11  1  -7 -1;
%!      9  68 -92  2;
%!      1  -4   8  1;
%!      1   5   1  1;
%!      2   1   2  8];
%!
%! I = [1  2  6  9;
%!      4  3  2  8;
%!      1  3  7  4;
%!      1  3  6  4;
%!      5  6  7  9;
%!      6  3  1  7;
%!      5  8  7  1;
%!      6  4  8  2;
%!      2  9  1  5;
%!      3  5  2 10];
%! 
%! b = [195.5 219 27.5 42 65.75 -25.75 200 10.25 43 95]';
%! omega = 0.95;
%! solution = [1 12.5 1 5 12 1.75 0.5 2 3.5 7]';
%! [x,i] = sor(V, I, b, omega);
%! assert(x, solution, 0.5e-10)

%!test
%! V = [100  0; 
%!      -10  1; 
%!       -2  2; 
%!       -6  3; 
%!        1 -1; 
%!     1087 29;
%!       57 -1.1;
%!        1 -6;
%!        9 70;
%!     -700 21];
%!
%! I = [ 1  5;
%!       2  6;
%!       3  1;
%!       4  2;
%!       5  3;
%!       6  4;
%!       7  9;
%!       8 10;
%!       9  8;
%!      10  7];
%! 
%! b = [200 127 2.4 30 1.2 7290 -6.4 8.1 246 702.1]';
%! omega = 0.99;
%! solution = [2 -12 0.8 -11 2 7 0.1 2.1 11 -1]';
%! [x,i] = sor(V, I, b, omega);
%! assert(x, solution, 0.5e-10)