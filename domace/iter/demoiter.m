## Solve and plot number of iterations for different omegas using SOR method.

## intialize random state
rand("state", [1 2 3 4 5]);

A = sortrows([rand(500,1) rand(500,500) + 500*eye(500)], [1])(:,2:501);
b = rand(500,1);
sol = A\b;
tol = 1e-5;
initv = rand(500,1);

iters = zeros(192, 3);
omegas = zeros(192, 1);
for i=1:192
  omega = i/100;
  [x, j, g, s] = iter(A, b, initv, tol, omega);
  iters(i,1) = j;
  iters(i,2) = g;
  iters(i,3) = s;
  omegas(i) = omega;
  assert(norm(sol-x, 'inf') < tol);
endfor
[m mind] = min(iters);
printf("Minimal SOR iterations: %d at omega = %f\n", m(3), omegas(mind(3)));
hold on;

## red   .. jacobi
## green .. gauss-seidel
## blue  .. SOR
plot(omegas, iters(:,1), 'or');
plot(omegas, iters(:,2), 'og');
plot(omegas, iters(:,3), 'ob');
