function [x,iters]=sor(A,b,x0,nat,omega)
  ## Funkcija SOR resi sistem enacb Ax=b z metodo successive over-relaxation
  ##
  ## Parametri:
  ## A     ... matrika
  ## b     ... vektor
  ## x0    ... poljubni začetni približek
  ## nat   ... pogoj za ustavitev iteracije
  ## omega ... faktor sproščanja
  ##
  ## Rezultat:
  ## x     ... rešitev Ax=b
  ## iters ... število potrebnih iteracij

  ## upper triangular, diagonal and lower diagonal
  U = triu(A, 1);
  D = diag(diag(A));
  L = tril(A, -1);

  ## precompute coefficients
  sig = D+omega*L;
  left = (omega*U + (omega-1)*D);
  right = sig\(omega*b);

  x = x0;
  for i=1:10000
    xn = right - sig\(left*x);
    n = norm(b-A*xn, 'inf');
    x = xn;
    if n < nat
      break
    endif
  endfor

  iters = i;
endfunction

%!test
%! A = [3 1 1; 1 8 5; 1 2 4];
%! b = [7 1 2]';
%! sol = A\b;
%! tol = 1e-5;
%! [x, _] = sor(A, b, [1 2 3]', tol, 0.9);
%! assert(norm(sol-x, 'inf') < tol);

%!test
%! rand("state", [1 2 3 4 5]);
%! A = rand(1000);
%! A = A + diag(sum(abs(A')));
%! b = rand(1000,1);
%! sol = A\b;
%! tol = 1e-5;
%! [x, _] = sor(A, b, rand(1000,1), tol, 0.91);
%! assert(norm(sol-x, 'inf') < tol);
