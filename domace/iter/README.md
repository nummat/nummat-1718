# Iterativne metode za diagonalno dominantne matrike


Napišite funkcijo `iter.m`, ki reši sistem $`A\mathbf{x}=\mathbf{b}`$ z
Jacobijevo, Gauss-Seidelovo in SOR iteracijo. Znano je, da omenjene 3
iteracije konvergirajo za matrike, ki so diagonalno dominantne po
vrsticah. Matrika je diagonalno dominantna po vrsticah, če velja

```math
|a_{ii}|\ge\sum_{j\not=i}|a_{ij}|,
```
za poljuben *i*. Funkcija iter naj preveri ali je mogoče matriko *A* s
permutacijami vrstic in stolpcev preurediti v diagonalno dominantno
matriko in tako preurejeno matriko uporabi za iteracijo. V primeru, da
matrike $A$ ni mogoče preurediti v diagonalno dominantno po vrsticah,
naj funkcija javi napako.

Klic funkcije naj bo oblike `[x,j,g,s]=iter(A,b,x0,nat,omega)`, kjer je

-   `x` rešitev sistema
-   `j,g,s` pa število iteracij, potrebnih za konvergenco pri
    Jacobijevi,

Gauss-Seidelovi in SOR iteraciji.

Pri vhodnih argumentih pa je

-   `x0` poljubni začetni približek,
-   `nat` pogoj za ustavitev iteracije in
-   `omega` parameter pri SOR iteraciji.

Iteracija naj se ustavi, ko je

```math
|A\mathbf{x}^{(k)}-\mathbf{b}|_\infty < \texttt{nat}.```
```

Funkcija demoiter najde vrednost parametra `omega`, pri katerem metoda najhitreje
konvergira za podano naključno matriko. Primer izhoda:
```
Minimal SOR iterations: 8 at omega = 0.910000
```
