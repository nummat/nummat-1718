function [x,j,g,s]=iter(A,b,x0,nat,omega)
  ## Funkcija iter resi sistem enacb Ax=b z Jacobijevo, Gauss-Seidelovo in SOR
  ## iteracijo.
  ##
  ## Parametri:
  ## A     ... matrika
  ## b     ... vektor
  ## x0    ... poljubni začetni približek
  ## nat   ... pogoj za ustavitev iteracije
  ## omega ... faktor sproščanja metode SOR
  ##
  ## Rezultat:
  ## x ... rešitev Ax=b
  ## j ... število iteracij do konvergence Jacobijeve iteracije
  ## g ... število iteracij do konvergence Gauss-Seidlove iteracije
  ## s ... število iteracij do konvergence iteracije SOR

  ## get diagonally dominant matrix and appropriately transposed b
  [AA, bb] = diag_dom(A, b);

  ## solve with iterative methods
  [sol1, j] = jacobi(AA,bb,x0,nat);
  [sol2, g] = gauss_seidel(AA,bb,x0,nat);
  [sol3, s] = sor(AA,bb,x0,nat,omega);

  ## sanity checks
  if norm(sol1-sol2, 'inf') > nat || norm(sol1-sol3, 'inf') > nat || norm(sol2-sol3, 'inf') > nat
    error("Not all methods converged")
  endif

  x = sol1;
end

%!test
%! A = [1 2 4; 3 1 1; 1 8 5];
%! b = [7 1 2]';
%! sol = A\b;
%! tol = 1e-5;
%! [x, j, g, s] = iter(A, b, [1 2 3]', tol, 1);
%! assert(norm(sol-x, 'inf') < tol);
%! assert(j >= 1)
%! assert(g >= 1)
%! assert(s >= 1)


## a bit bigger
%!test
%! rand("state", [1 2 3 4 5]);
%! A = sortrows([rand(100,1) rand(100,100) + 100*eye(100)], [1])(:,2:101);
%! b = rand(100,1);
%! sol = A\b;
%! tol = 1e-5;
%! initv = rand(100,1);
%!
%! for i=1:192
%!   omega = i/100;
%!   [x, j, g, s] = iter(A, b, initv, tol, omega);
%!   assert(norm(sol-x, 'inf') < tol);
%! endfor
