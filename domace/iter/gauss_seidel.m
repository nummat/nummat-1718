function [x,iters]=gauss_seidel(A,b,x0,nat)
  ## Funkcija gauss_seidel resi sistem enacb Ax=b z Gauss-Seidelovo iteracijo
  ##
  ## Parametri:
  ## A     ... matrika
  ## b     ... vektor
  ## x0    ... poljubni začetni približek
  ## nat   ... pogoj za ustavitev iteracije
  ##
  ## Rezultat:
  ## x     ... rešitev Ax=b
  ## iters ... število potrebnih iteracij

  L = tril(A);
  U = A - L;
  x = x0;

  for i=1:10000
    xn = L\(b - U*x);
    n = norm(b-A*xn, 'inf');
    x = xn;
    if n < nat
      break
    endif
  endfor

  iters = i;
endfunction

%!test
%! A = [3 1 1; 1 8 5; 1 2 4];
%! b = [7 1 2]';
%! sol = A\b;
%! tol = 1e-5;
%! [x, _] = gauss_seidel(A, b, [1 2 3]', tol);
%! assert(norm(sol-x, 'inf') < tol);

%!test
%! rand("state", [1 2 3 4 5]);
%! A = rand(1000);
%! A = A + diag(sum(abs(A')));
%! b = rand(1000,1);
%! sol = A\b;
%! tol = 1e-5;
%! [x, _] = gauss_seidel(A, b, rand(1000,1), tol);
%! assert(norm(sol-x, 'inf') < tol);
