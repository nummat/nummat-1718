function [A,a]=diag_dom(B,b)
  ## Funkcija diag_dom matriko B s permutacijami vrstic poskusi pretvoriti v
  ## diagonalno dominantno matriko. Če to ni mogoče, javi napako. Na isti način
  ## permutira tudi elemente vektorja b.
  ##
  ## Parametri:
  ## B     ... matrika
  ## b     ... vektor
  ##
  ## Rezultat:
  ## A     ... diagonalno dominantna matrika
  ## a     ... vektor


  ## find indices of dominant elements
  rows=[];
  for i=1:length(B)
    [ind, greater]=line_index_max(B(i,:));

    ## no row element is dominant
    if (!greater)
      error("Ni dominantnega elementa v vrstici.")
    endif

    rows(i)=ind;
  endfor

  ## check that there is a dominant element for each column
  if (length(unique(rows)) != length(rows))
    error("Dominantni elementi v istem stolpcu.")
  endif

  ## sort and return
  sorted = sortrows([rows' B b], [1]);
  n = length(sorted);
  A = sorted(:,2:n-1);
  a = sorted(:, n);
endfunction

## happy case
%!test
%! A = [1 2 4; 6 3 2; 3 8 4];
%! a = [1 2 3]';
%! [B, b] = diag_dom(A,a);
%! assert(B, [6 3 2; 3 8 4; 1 2 4]);
%! assert(b, [2 3 1]');

## line without dominant element
%!error <Ni dominantnega elementa v vrstici.> diag_dom([1 2 4; 6 3 4; 3 8 4], [1 2 3]');

## dominant elements in the same column
%!error <Dominantni elementi v istem stolpcu.> diag_dom([1 2 4; 1 2 4; 3 8 4], [1 2 3]');
