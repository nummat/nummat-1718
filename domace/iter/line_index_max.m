function [i, greater]=line_index_max(row)
  ## Funkcija vrne indeks najvecjega elementa v vektorju in če je |element| ≥ ∑|preostanek vrstice|
  ##
  ## Parametri:
  ## row ... vektor
  ##
  ## Rezultat:
  ## i       ... indeks prvega najvecjega elementa
  ## greater ... najvecji element absolutno vecji od absolutne vsote preostanka vektorja

  absr = abs(row);
  [v, i] = max(absr);
  absr(i) = 0;
  greater = v >= sum(absr);
endfunction

## happy case
%!test
%! [i,greater] = line_index_max([1 3 5]);
%! assert(i, 3);
%! assert(greater);

## happy case, another index
%!test
%! [i,greater] = line_index_max([1 5 4]);
%! assert(i, 2);
%! assert(greater);

## sum not greater
%!test
%! [i,greater] = line_index_max([5 4 2]);
%! assert(i, 1);
%! assert(greater, false);

## sum not greater for abs values
%!test
%! [i,greater] = line_index_max([5 -4 2]);
%! assert(i, 1);
%! assert(greater, false);

## sum greater for negative values
%!test
%! [i,greater] = line_index_max([-2 -5 -2]);
%! assert(i, 2);
%! assert(greater, true);

## sum not greater for negative values
%!test
%! [i,greater] = line_index_max([-5 -4 -2]);
%! assert(i, 1);
%! assert(greater, false);
