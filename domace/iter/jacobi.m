function [x,iters]=jacobi(A,b,x0,nat)
  ## Funkcija jacobi resi sistem enacb Ax=b z Jacobijevo iteracijo
  ##
  ## Parametri:
  ## A     ... matrika
  ## b     ... vektor
  ## x0    ... poljubni začetni približek
  ## nat   ... pogoj za ustavitev iteracije
  ##
  ## Rezultat:
  ## x     ... rešitev Ax=b
  ## iters ... število potrebnih iteracij

  D = diag(diag(A));
  R = A - D;
  x = x0;

  for i=0:10000
    xn = D\(b - R*x);
    n = norm(b-A*xn, 'inf');
    x = xn;
    if n < nat
      break
    endif
  endfor

  iters = i;
endfunction

%!test
%! A = [3 1 1; 1 8 5; 1 2 4];
%! b = [7 1 2]';
%! sol = A\b;
%! tol = 1e-5;
%! [x, _] = jacobi(A, b, [1 2 3]', tol);
%! assert(norm(sol-x, 'inf') < tol);

%!test
%! rand("state", [1 2 3 4 5]);
%! A = rand(500);
%! A = A + diag(sum(abs(A')));
%! b = rand(500,1);
%! sol = A\b;
%! tol = 1e-5;
%! [x, _] = jacobi(A, b, rand(500,1), tol);
%! assert(norm(sol-x, 'inf') < tol);
