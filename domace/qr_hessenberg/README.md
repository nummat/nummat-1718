Naslov domače naloge
===========================

Navodila (zbriši pred oddajo)
----------

Ustvarite svojo vejo v `git` repozitoriju z ukazom

    git branch ime_naloge_vzdevek

Popravite ta README, da bo vseboval besedilo naloge, kratek opis rešitve, kakšen
primer uporabe in kakšno sliko. Če nimate res dobrega razloga, ne ustvarjajte
novih direktorijev, vse datoteka naj bodo kar v tem direktoriju.

Spremembe lahko večkrat zapišete (`git commit`) in tudi potisnete (`git push`)
na Gitlab, saj ste v svoji veji. Ko bi radi oddali nologo, v Gitlabu naredite
`merge request` in v opisu prosim omenite moje ime (`@mrcinv`), da bom dobil
obvestilo. Na primer takole: 

    @mrcinv: Prosim, da pregledate domačo nalogo.
