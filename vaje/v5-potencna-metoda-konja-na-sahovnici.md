Vaja 5 - potenčna metoda za iskanje lastnih vrednosti
=========================

Danes bomo spoznali

1.  kako napisatni sistem enačb za praktičen primer
2.  primerno izbiro metode za reševanje
3.  implementacija v programskem jeziku in težave

Konji na šahovnici
=======================================


Problem
-------

Konj naključno skače po šahovnici. Katera polja bolj pogosto obišče?

Kaj pa če po šahovnici skačeta 2 konja? Prej ali slej bo en konj pojedel
drugega. Na katerih poljih se bo to najverjetneje zgodilo?

Problem modeliramo z [Markovskimi
verigami](https://en.wikipedia.org/wiki/Markov_chain). Prostor stanj Markovske
verige je v primeru enega konja polje na šahovnici, v primeru dveh konjev pa par
šahovskih polj. Markovsko verigo predstavimo z matriko prehodnih verjetnosti.

Limitna porazdelitev Markovske verige je lastni vektor transponirane matrike
prehodnih verjetnosti za lastno vrednost 1.

```math
P\vec{p}=\vec{p}
```

Naloga
------

Napiši program, ki poišče lastni vektor za problem konja na šahovnici s potenčno
metodo.
