# Spekralno razvrščanje v gruče

Pokazali bomo metodo razvrščanja v gruče, ki uporabi spektralno analizo
Laplaceove matrike podobnostnega grafa podatkov, zato da podatke preslika v
prostor, kjer jih je lažje razvrstiti.

## Podobnostni graf in Laplaceova matrika
Podatke (množico točk v $`\mathbb{R}^n`$) želimo razvrstiti v več gruč. Najprej
ustvarimo *podobnostni uteženi graf*, ki povezuje točke, ki so si v nekem smislu
blizu. Podobnostni graf lahko ustvarimo na več načinov:
- **ε-okolice**: s točko *xᵢ* povežemo vse točke, ki ležijo v ε-okolici te točke
- **k-najbližji sosedi**: *xₖ* povežemo z *xᵢ*, če je *xₖ* med *k* najbližjimi
točkami. Tako dobimo usmerjen graf, zato ponavadi upoštevmo povezavo v obe
smeri.
- **poln utežen graf**: povežemo vse točke, vendar povezave utežimo glede na
  razdaljo. Pogosto uporabljena utež je nam znana radialna bazna funkcija

```math
w(x_i, x_k) = \exp\left(-\frac{\|x_i-x_k\|^2}{2\sigma^2}\right)
```
pri kateri s parametrom $`\sigma`$ lahko določamo velikost okolic.

Grafu podobnosti priredimo matriko uteži
```math
W =[w_{ij}],
```
in Laplaceovo matriko 
```math
L = D-W
```
kjer je $`D=[d_{ij}]`$ diagonalna matrika z elemetni 
$`d_{ii}=\sum_{j}w_{ij}`$.
Laplaceova matrika je simetrična, nenegativno definitna in ima vedno eno lastno
vrednost 0 za lastni vektor iz samih enic.

## Algoritem

Velja naslednji izrek, da ima Laplaceova matrika natanko toliko lastnih
vektorjev za lastno vrednost 0, kot ima graf komponent za povezanost. Na prvi
pogled se zdi, da bi lahko bile komponente kar naše gruče, a se izkaže, da to ni
najbolje. 

 - Poiščemo *k* najmanjših lastnih vrednosti za Laplaceovo matriko in izračunamo
njihove lastne vektorje. 
 - Označimo matriko lastnih vektorjev *Q=[v₁, v₂, ...,
vₖ]*. Stolpci *Qᵀ* ustrezajo koordinatam točk v novem prostoru. 
 - Za stolpce matrike *Qᵀ* izvedemo nek drug algoritem gručenja (npr. algoritem
   *k* povprečij).
   

## Primer
Algoritem preverimo na mešanici treh gaussovih porazdelitev

```octave
m = 200;
x = [1+randn(m, 1); -3+randn(m,1); 2*randn(m,1)]; 
y = [-2+randn(m, 1); 1+2*randn(m,1); 1+0.5*randn(m,1)];
plot(x, y, '.')
```

## Literatura

 - Ulrike von Luxburg [A Tutorial on Spectral
   Clustering](https://arxiv.org/abs/0711.0189)
 - Peter Arbenz [Lecture Notes on Solving Large Scale Eigenvalue Problems](http://people.inf.ethz.ch/arbenz/ewp/Lnotes/lsevp.pdf)
