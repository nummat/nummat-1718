Vaja 3 - linearni sistemi
=========================

Danes bomo spoznali

1.  kako napisatni sistem enačb za praktičen primer
2.  primerno izbiro metode za reševanje
3.  implementacija v programskem jeziku in težave

Opis krivulj z implicitno interpolacijo
=======================================

Iz množice točk želimo rekonstruirati krivuljo, ki gre skozi te točke.
Krivulje v ravnini lahko opišemo na različne načine

1.  **eksplicitno**: $`y=f(x)`$
2.  **parametrično**: $`(x,y) = (x(t),y(t))`$
3.  **implicitno** z enačbo $`F(x,y)=0`$

Tokrat se bomo posvetili implicitni predstavitvi krivulje.

Problem
-------

Imamo točke v ravnini s koordinatami
$`(x_1,y_1),(x_2,y_2),\ldots, (x_n,y_n)`$. Iščemo krivuljo, ki gre skozi
vse točke. Po možnosti naj bo krivulja gladka, poleg tega ni nujno, da
do zaporedne točke v seznamu, tudi zaporedne točke na krivulji. Krivuljo
iščemo v **implicitni** obliki, torej v obliki enačbe

```math
F(x,y) = 0.
```

Iskano krivuljo bomo zapisali kot ničto nivojnico neke funkcije
$`F(x,y)`$. Iščemo torej funkcijo $`F(x,y)`$, za katero velja

```math
F(x_i,y_i) = 0\quad i\le n.
```


Ta pogoj žal ne zadošča. Dodamo moramo še nekaj točk, ki so znotraj
območja omejenega s krivuljo. Označimo jih z
$`(x_{n+1},y_{n+1}),\ldots,(x_m,y_m)`$, v katerih predpišemo vrednost $1$

```math
F(x_i,y_i) = 1\quad i\ge n+1.
```

Naloga
------

Napiši program, ki za dane točke poišče interpolacijsko funkcijo oblike
```math
F(\mathbf{x})=\sum_i d_i\phi(\mathbf{x}-\mathbf{x}_i)+P(\mathbf{x}),
```

kjer so

-   $`\mathbf{x} = (x,y)`$
-   $`P(\mathbf{x})`$ polinom stopnje 1 (linearna funkcija v $`x`$ in $`y`$)
-   $`d_i`$ primerno izbrane uteži.
-   $`\phi`$ radialna bazna funkcija, ki je odvisna zgolj od razdalje do *i*-te
    točke $`r = \|\mathbf{x}-\mathbf{x}_i\|`$.
    * "thin plate": $`\phi(r)=|r|^2\log(|r|)`$ za 2D 
    * Gaussova: $`\phi(r)=\exp(-r^2/\sigma^2)`$ 
    * racionalni približek za Gaussovo

```math
\phi(r)=\frac{1}{1+r^{2p}}
```

