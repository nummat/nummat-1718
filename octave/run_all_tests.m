## Izvede teste za vse datoteke, ki imajo teste, v danem direktoriju

function s = run_all_tests(directory)
  s = true;
  m_files = dir([directory "/*.m"]);
  for i = 1:length(m_files)
    f = [directory "/" m_files(i).name];
    if has_tests(f)
      s = test(f) & s;
    end
  end
endfunction

function retval = has_tests (f)

  fid = fopen (f);
  if (fid < 0)
    error ("runtests: fopen failed: %s", f);
  endif

  str = fread (fid, "*char").';
  fclose (fid);
  retval = ! isempty (regexp (str, '^%!(?:test|xtest|assert|error|warning)',
                              'lineanchors', 'once'));

endfunction
