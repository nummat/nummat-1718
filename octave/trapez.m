function [y, x] = trapez(f, y0, x0, xk, n, it)
% Izračuna približek za rešitev začetnega problema za
% diferencialno enačbo y' = f(y,x) z začetnim pogojem
% y(x0) = y0 na intervalu [x0, xk] z n koraki 
% implicitne trapezne metode

y = zeros(1,n+1);
x = linspace(x0, xk, n+1);
h = x(2) - x(1);
y(1) = y0;
for i=1:n
  fi = f(y(i), x(i));
  y(i+1) = y(i) + h*fi; % začetni približek
  for k=1:it
    y(i+1) = y(i) + h/2*(f(y(i+1), x(i+1)) + fi);
  end
end

%!test
%! f = @(y, x) x + y;
%! y0 = 1; x0 = 0; x1 = 1; h = 1;
%! [y, x] = trapez(f, y0, x0, x1, 1, 10);
%! assert(size(y), [1, 2])
%! assert(size(x), [1, 2])
%! assert(y(2), (y0 + h/2*(x0+x1+y0))/(1-h/2), 1e-2)