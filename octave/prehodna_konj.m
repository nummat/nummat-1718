function P = prehodna_konj(n,m)
% Vrne prehodno matriko za Markovsko verigo, ki 
% predstavlja naključno skakanje konja po šahovnici 
% n x m

N = n*m; % dimenzija matrike

skoki = [1 1 2 2 -1 -1 -2 -2;
              2 -2 1 -1 2 -2 1 -1];
skoki_k = (skoki(1,:) - 1)*n + skoki(2,:);
% zanka po vrsticah matrike P
P = sparse(N, N);
for i=1:n
   for j=1:m
      % indeks v matriki P
      k = m*(i - 1) + j;
      for l = 1:8
        skok = skoki(:,l);
        nov_i = i + skok(1);
        nov_j = j + skok(2); 
        % če skocimo v polje
        if (0<nov_i) &&(nov_i<=n) &&(0<nov_j) &&(nov_j<=m)
           p = (nov_i - 1)*m + nov_j;
           P(k, p) = 1;
        end
     end
     % normiramo vrstico
     if any(P(k,:))
       P(k,:) = P(k,:)/sum(P(k,:));
     end
   end
 end
