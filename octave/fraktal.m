function M = fraktal(F, JF, meje, nx, ny)
  # funkcija nariše območja konvergence za Newtonovo
  # metodo za sistem enačb F(x) = 0
  tol = 1e-5;
  maxit = 50;
  M = zeros(ny, nx);
  x = linspace(meje(1), meje(2), nx);
  y = linspace(meje(3), meje(4), ny);
  nicle = [];
  for i = 1:nx
    for j = 1: ny
    x0 = [x(i); y(j)];                      # začetni približek
    [nicla, it] = newton(F, JF, x0, tol, maxit);
    if it >= maxit
      M(i,j) = 0;
      continue
    endif
    nova =  true;
     for k = 1:size(nicle, 2)
       if norm(nicla - nicle(:,k))<2*tol
         M(i, j) = k;
         nova = false;
         break
       endif
     endfor
     if nova
       nicle = [nicle nicla];
       M(i, j) = size(nicle, 2);
     endif
   endfor
 endfor
endfunction

%!demo
%! F = @(x) [x(1)^3-3*x(1)*x(2)^2-1; 3*x(1)^2*x(2) - x(2)^3];
%! JF = @(x) [ 3*(x(1)^2-x(2)^2), -6*x(1)*x(2) ; 6*x(1)*x(2) , 3*(x(1)^2-x(2)^2)];
%! M = fraktal(F, JF, [-2, 2, -2, 2], 200, 200)
%! image(10*M)