function alfa = impl_interp(tocke, vrednosti)
% Funkcija  alfa = impl_interp(tocke, vrednosti)
% izračuna koeficiente interpolacijske funkcije F
% ki je podana kot linearna kombinacija RBF
% Parametri:
% tocke ... 2xn matrika s koordinatami točk
% vrednosti ... nx1 vektor s predpisanimi vrednostmi
% Rezultat:
% alfa ... vektor koeficientov
%
% F(x) = sum_i(exp(-norm(x - tocke(:,i))^2)*alfa(i))

% generiraj matriko
[m, n] = size(tocke);
A = zeros(n,n);
for j=1:n
  for i=1:n
    A(i,j) = exp(-norm(tocke(:,i)-tocke(:,j))^2);
  end
end

% reši sistem
alfa = A\vrednosti;