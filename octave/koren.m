function [y, dy] = koren(x)
% Funkcija y = koren(x)
% izračuna kvadratni koren števila x
% na 10 decimalnih mest natančno
% poleg vrednosti kvadratnega korena
% funkcija vrne tudi odvod korenske funkcije
% v točki x


                                % začetni približek dobimo tako, da razpolovimo eksponenet
bits = bitunpack(x);
                                % eksponent
e = bitpack([bits(end-11:end-1) false false false false false], "int16");
e = e - 1023; % bias
y = 2^(double(e/2));
dy = 0; % bitnih funkcij ne znamo odvajati, zato zamižimo na eno oko

% newtonova metoda
for i=1:10
   y = (y + x./y)/2;
   dy = (dy + (y-dy.*x)./y.^2)/2; % y1' = ((y0 + x/y0)/2)'
end

%!test
%! tol = 0.5e-10; % absolutna napaka
%! assert(koren(4),2,tol)
%! assert(koren(1/4), 0.5, tol)
%!test % velika in majhna števila 
%! assert(koren(1e-10),1e-5,5e-11)
%! assert(koren(1e10),1e5,5e-11)
%!test %relativna napaka
%! tol = 0.5e-10;
%! assert(koren(1e-10),1e-5,-tol)
%!test %odvod
%! [y, dy] = koren(4);
%! assert(dy, 0.25, 5e-11)
