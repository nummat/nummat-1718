% konvergenca eulerjeve metode
prava = @(x) exp(-cos(x)+1);
f = @(y, x) y.*sin(x);

n = 2.^(1:15);
napaka_e = [];
napaka_t = [];

for k = 1:15
  [y, x] = euler(f, 1, 0, 10, n(k));
  napaka_e = [napaka_e, norm(y - prava(x), 'inf')];
  [y, x] = trapez(f, 1, 0, 10, n(k), 3);
  napaka_t = [napaka_t, norm(y - prava(x), 'inf')];
end

plot(log10(n), log10(napaka_e), '*r')
hold on
plot(log10(n), log10(napaka_t), 'ob')
hold off
title("Napaka Eulerjeve in trapezne metode v odvisnosti od števila korakov");