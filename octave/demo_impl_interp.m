tocke = [-1 1 2 1; -1 -1 1 0];
vrednosti =[0.1 0.1 0.1 1]';
alfa = impl_interp(tocke, vrednosti);

plot(tocke(1,:), tocke(2,:),'o')

x = linspace(-3,4,30);
y = linspace(-3,3,30);
[X,Y] = meshgrid(x,y);

Z = impl_interp_vred(tocke, alfa, X, Y);

hold on
contour(x,y,Z-0.1,0:1)
hold off

impl_interp_vred(tocke, alfa, tocke(1,:), tocke(2,:))

tocke = [-1 1 1 -1 -0.5 0.5 0.5 -0.5; 
              -1 -1 1 1 -0.5 -0.5 0.5 0.5];
vrednosti = [0 0 0 0 1 1 1 1]' + 0.1;
alfa = impl_interp(tocke, vrednosti);

figure
plot(tocke(1,:), tocke(2,:),'o')

x = linspace(-3,3,30);
y = linspace(-3,3,30);
[X,Y] = meshgrid(x,y);

Z = impl_interp_vred(tocke, alfa, X, Y);

hold on
contour(x,y,Z-0.1,0:1)
hold off
