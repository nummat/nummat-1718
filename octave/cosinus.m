function y = cosinus(x)
  % Funkcija y = cosinus(x) izračuna vrednosti cos(x).
  
  x = mod(x, 2*pi); % perioda
  if x <= pi/4
   taylor = [2.08767569878681e-09
   -2.75573192239859e-07
   2.48015873015873e-05
   -1.38888888888889e-03
   4.16666666666667e-02
   -5.00000000000000e-01
   1.00000000000000e+00];
   y = polyval(taylor,x*x);
  elseif x > pi
    y = cosinus(2*pi-x); % cos(2pi-x) = cos(x)
  elseif x > pi/2
    y = -cosinus(pi-x);
  else
    y = sinus(pi/2 - x);
  end
%!test
%! tol = 5e-11;
%! assert(cosinus(pi/3), 0.5, -tol)
%! assert(cosinus(pi), -1, -tol)
%! assert(cosinus(-pi/3), 0.5, -tol)
%! assert(cosinus(10*pi+pi/4), 1/sqrt(2), -tol)
%! assert(cosinus(pi/6), sqrt(3)/2, -tol)