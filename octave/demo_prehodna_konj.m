n = 8;
m = 8;
N = m*n;
tol = 1e-5;
Pt = prehodna_konj(n, m)';
% Potencna metoda
x = rand(N,1);
for i = 1: 1000
  x0 = Pt*(Pt*x);
  x0 = x0/norm(x0,1);
  if norm(x-x0,'inf') < tol
      break
  end
  x = x0;
end
% ker ima P' dve lastni vrednosti 1 in -1
% lahko lastni vektor za 1 dobimo kot x +Px

x = x + Pt*x;  % lastna vrednost za 1
p = x/sum(x); % normiramo, da je vsota 1

p = reshape(p, m, n)';

imshow(p, [0,max(max(p))],"colormap", hot)