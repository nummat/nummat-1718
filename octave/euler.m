function [y, x] = euler(f, y0, x0, xk, n)
% Izračuna približek za rešitev začetnega problema za
% diferencialno enačbo y' = f(y,x) z začetnim pogojem
% y(x0) = y0 na intervalu [x0, xk] z n koraki 
% Eulerjeve metode

y = zeros(1,n+1);
x = linspace(x0, xk, n+1);
h = x(2) - x(1);
y(1) = y0;
for i=1:n
  y(i+1) = y(i) + h*f(y(i), x(i));
end
%!test
%! f = @(y, x) x + y;
%! y0 = 1; x0 = 0; x1 = 1;
%! [y, x] = euler(f, 1, 0, 1, 1);
%! assert(size(y), [1, 2])
%! assert(size(x), [1, 2])
%! assert(y(1), y0)
%! assert(y(2), y0 + (x1-x0)*(x0+y0), eps)
