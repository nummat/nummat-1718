function [A, b] = matrika_laplace(X)
% funkcija vrne matriko sistema A in desne strani B za Laplaceovo enačbo na kvadratu
% [0,1]x[0,1]
% X ... n x n matrika, ki vsebuje robne vrednosti

[n, m] = size(X); 		% velikost pravokotne mreže
k = @(i, j) i-1 + (j-2)*(n-2); 	% preslikava med dvojnimi in enojnim indeksom
d = (n-2)*(m-2); 		% dimenzija sistema
A = zeros(d);
b = zeros(d, 1);

sosedi = [1 0; -1 0; 0 1; 0 -1]';
for i = 2:n-1
    for j = 2:m-1
	for s = sosedi
	   if (i+s(1)<2) || (i+s(1)>(n-1)) || (j+s(2)<2) || (j+s(2)>(m-1)) % sosed je na robu
	      b(k(i,j)) = b(k(i,j)) - X(i+s(1), j+s(2));
      	   else
	      A(k(i,j), k(i+s(1), j+s(2))) = 1;
	   end
	   A(k(i,j), k(i,j)) = -4;
	endfor
   endfor
endfor

%!test
%! X = [1 2 3; 4 5 6; 7 8 9];
%! [A, b] =  matrika_laplace(X);
%! assert(A, -4)
%! assert(b, -20)
