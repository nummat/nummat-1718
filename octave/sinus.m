function y = sinus(x)
  % Funkcija y = sinus(x) izračuna vrednosti sin(x).
  
  x = mod(x, 2*pi); % perioda
  if x <= pi/4
   taylor = [1.60590438368216e-10
   -2.50521083854417e-08
   2.75573192239859e-06
   -1.98412698412698e-04
   8.33333333333333e-03
   -1.66666666666667e-01
   1.00000000000000e+00];
   y = x*polyval(taylor,x*x);
  elseif x > pi
    y = -sinus(2*pi-x); % sin(2pi-x) = -sin(x)
  elseif x > pi/2
    y = sin(pi-x);
  else
    y = cosinus(pi/2 - x);
  end
%!test
%! tol = 5e-11;
%! assert(sinus(pi/6), 0.5, -tol)
%! assert(sinus(pi),0, tol)
%! assert(sinus(-pi/6), -0.5, -tol)
%! assert(sinus(10*pi+pi/4), 1/sqrt(2), -tol)
%! assert(sinus(pi/3), sqrt(3)/2, -tol)