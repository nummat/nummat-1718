function I = ndquad(omega, x0, f, d)
  % Funkcija I = ndquad(omega, x0, f, d) izračuna
  % d-kratni integral funkcije f s kvadraturno formulo
  % sum omega(i1)*...*omega(id)*f([x0(i1),...,x0(id)])
  % omega ... uteži za kvadraturno formulo za navaden integral
  % x0 ... vozlišča kvadraturne formule
  % f ... kazalec na funkcijo
  % d ... večkratnost integrala (dimenzija)
  n = length(x0) % št. vozlišč
  I = 0;
  indeksi = ones(d,1);
  for k = 1:n^d
    I = I + prod(omega(indeksi))*f(x0(indeksi));
    for j = d:-1:1
      if indeksi(j)>=n
        indeksi(j) = 1;
      else
        indeksi(j) = indeksi(j)+1;
        break
      end
    end
  end
  %for i=1:n
  %  for j=1:n
  %    I = I + omega(i)*omega(j)*f([x0(i),x0(j)]);
  %  end
  %end
endfunction

%!test
%! % integriramo x+y na [0,1]^2 s Simpsonovim pravilom
%! I = ndquad([0.5 2 0.5]/3, [0 0.5 1], @(x) x(1)+x(2), 2);
%! assert(I,1, eps);

%!demo
%! % povprečna razdalja med dvema točkama v kocki [0,1]^3
%! f = @(x) norm(x(1:3)-x(4:6));
%! % Sestavljeno Simpsonovo pravilo na 6 točkah
%! x0 = linspace(0,1,7);
%! omega = (x0(2)-x0(1))/3*[1 4 2 4 2 4 1];
%! tic; I = ndquad(omega, x0, f, 6); toc
%! I
%! % gaussove kvadrature na 5 točkah (vir Wikipedia)
%! x0 = [0.538469, -0.538469, 0, 0.90618, -0.90618];
%! omega = [ 0.478629 0.478629 0.568889 0.236927 0.236927];
%! omega = omega/2; % dx = (b-a)/2 dt
%! x0 = x0/2 + 0.5; % x = (b-a)/2*t + (b+a)/2
%! tic; Igauss = ndquad(omega, x0, f, 6); toc
%! Igauss