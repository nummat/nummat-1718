function Z = impl_interp_vred(tocke, alfa, X,Y)
% Funkcija Z = impl_interp_vred(tocke, alfa, X,Y)
% izračuna vrednosti interpolacijske funkcije podane
% z RBF v točkah s koordiantami X,Y

Z = zeros(size(X));
n = length(tocke);
for i=1:n
  Z = Z + alfa(i)*exp(-(X-tocke(1,i)).^2-(Y-tocke(2,i)).^2);
end