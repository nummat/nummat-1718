h = 2.^(-(1:50));
napaka = [];
for hi = h
  napaka = [napaka 
    prava_vrednost-(koren(3+hi)-koren(3-hi))/(2*hi)];
end
plot(log10(h), log10(abs(napaka)))