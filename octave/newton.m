function [x, it] = newton (F, JF, x0, tol, maxit)
  # Reši nelinearen sistem enačb F(x) = 0 z
  # z Newtonovo metodo
  # F ... kazalec na funkcijo, ki poda leve strani enačb
  # JF ... Jacobija matrika odvodov F
  # x0 ... začetni približek
  # tol ... natančnost (kdaj se iteracija ustavi)
  # maxit ... največje število korakov
  # x ... približek za rešitev
  # it ... število korakov iteracije
it = 0;
razlika = inf;
while (norm (razlika, 'inf') > tol) && (it <=maxit)
   x = x0 - JF (x0)\F (x0);
   razlika = x - x0;
   x0 = x;
   it = it +1;
end

%!test
% [x, it] = newton(@(x) x-tan(x), @(x) 1-1/cos(x)^2, 5*pi/2-0.1, 1e-10, 100);
% assert(abs(x-tan(x))<1e-10)
% assert(it<10)