# Vaje pri predmetu Numerična matematika
[![status testov](https://gitlab.com/nummat/nummat-1718/badges/master/build.svg)](https://gitlab.com/nummat/nummat-1718/pipelines)
# Šolsko leto 2017/18

## Sodelujoči
 * Martin Vuk (@mrcinv)
 * Nejc Kišek (@thenejcar)
 * Dejan Benedik (@thejan2009)
 * Peter Fajdiga (@peterfajdiga)
 * Julija Petrič (@jp8874)
 * Ajda Lampe (@lampeajd)
 * Roman Komac (@RomanKomac)

# Program vaj

## Uvod

### Vaja 1
Računanje kvadratnega korena s Taylorjevo vrsto in Newtonovo metodo.

### Vaja 2
Računajne vrednosti funkcij *sin* in *cos*.

## Linerani sistemi

### Vaja 3
Implicitna interpolacija oblik z 
[radialnimi baznimi funkcijami](https://en.wikipedia.org/wiki/Radial_basis_function).

### Vaja 4
Ravnovesna lega mreže vzmeti.

## Lastne vrednosti
### Vaja 5
Invariantna porazdelitev [Markovske verige](https://en.wikipedia.org/wiki/Markov_chain)

### Vaja 6
Spektralno razvrščanje v gruče

## Interpolacija in aproksimacija
### Vaja 7
Newtonova interpolacija, Hermitova interpolacija
### Vaja 8

## Nelinearne enačbe
### Vaja 9
Konvergenčno območje Newtonove metode
## Integracija
### Vaja 10
Metoda nedoločenih koeficientov in sestavljene kvadraturne formule
### Vaja 11
Večkratni integrali
## Odvod
### Vaja 12
Numerični odvod, metoda končnih razlik za Laplaceovo enačbo (minimalne ploskve)
## Diferencialne enačbe
### Vaja 13
Eulerjeva in trapezna metoda.
### Vaja 14
Perioda geostacionarne orbite