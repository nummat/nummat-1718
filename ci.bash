#!/usr/bin/env bash

# This script iterates over all directories containing .m files, runs any
# availabe tests and reports the results.

# directories that contain .m files
__dirs=$(find . -type f -name '*.m' -exec dirname {} \; | uniq);

# exit status
_out=0

for d in $__dirs; do
    echo "folder: ${d:2}"

    # nice display
    octave -fq --no-gui --path=${d:2} --eval "runtests(\"${d}\")";

    # rerun to get exit status
    octave -fq --no-gui --path=octave:${d:2} --eval "s = run_all_tests(\"$d\"); exit(!s);" || _out=1;
done
exit $_out
