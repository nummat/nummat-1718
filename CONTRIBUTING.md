# Kako sodelovati pri predmetu Numerična matematika

Ta repozitorij je namenjem zbiranju gradiv pri predmetu Numerična matematika.
Želimo si, da skupaj ustvarimo lepo urejen in zaokrožen repozitorij 
z vsemi gradivi, ki jih bomo ustvarili na vajah in z domačimi nalogami. Zato ste 
vsi študenti, ki ste vpisani na ta predmet vabljeni, da se pridružite temu 
projektu in sodelujete tudi na tak način.

## Laboratorijske vaje
Na laboratorijskih vajah bomo iskali ravnotežje med razlago na tablo in 
programiranjem asistenta in samostojnim delom študentov. Vaje bodo 
(upam) uravnotežena mešanica:
 
 * razlage na tablo
 * programiranja 
 * nekaj samostojnega programiranja
 
Del svojih obveznosti lahko študent opravi že na vajah, če za naloge, ki jih 
bomo reševali na vajah, izdela rešitev in poda zahtevo za združitev 
/merge request/.

## Domače naloge/sprotno delo

Domače naloge oziroma sprotno delo bo potekalo sodelovalno. To pomeni, da bomo 
skupaj razvijali knjižnico numeričnih funkcij v tem repozitoriju. Poleg tega 
naj bi vsak študent naredil eno nalogo v svojem repozitoriju. 

Za pozitivno oceno naj bi študent prispeval več stvari v eni od naslednjih 
oblik:

 - izdelan lasten gitlag/github/... projekt z dokumentacijo, kodo in testi
   - vsak študent naj bi izdelal en tak projekt 
 - sprejeti pull requesti na skupnem projektu
 - sodelovanje na vajah
   - pisanje kode
   - pisanje dokumentacije
   - pisanje testov
 - domače naloge
 - sodelovanje preko zaznamkov (issues)

Ocena se določi na podlagi kvalitete prispevkov
 
  - na oceno vplivajo le dobri prispevki, če kdo kdaj pošlje kakšno neumnost, 
     se mu to ne šteje v minus
  - ko študent zbere dovolj prispevkov, mu asistent dodeli oceno
  - ocena se lahko le še popravi z bolj kvalitetnimi prispevki

### Kako oddati domačo nalogo
Domače naloge oddajte kot [merge request](https://gitlab.com/help/user/project/merge_requests/index.md). 
Spodaj je zelo na kratek opis, kako to naredite. Predpostavljam, da ste vsaj malo
vešči z orodjem [Git](https://git-scm.com/).
### Delo z izvorno kodo z Git-om

 - najprej si na svojem računalniku ustvarite clon repozitorija

```
git clone https://gitlab.com/nummat/nummat-1718.git
cd nummat-1718
```

 - nato ustvarite novo [vejo](https://gitlab.com/help/user/project/repository/branches/index.md)

```
git branch nickname-dn1
git checkout nickname-dn1
```

 - nalogo rešite in sproti spremembe z `git commit` beležite v repozitorij.
 - nalogo prenesete na strežnik z ukazom `git push`

```
git push origin nickname-dn1
```
### Merge request na Gitlab
Ko ste svojo rešitev dokončali in jo uspešno prenesli na gitlab, lahko ustvarite 
[merge request](https://gitlab.com/nummat/nummat-1718/merge_requests/new). 
Za *source* izberete svojo vejo, za *target* pa vejo `master`.
## Viri

- [priporočila za Gitlab](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)
  - [11 pravil za Gitlab](https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/)
- [Kako v kodo dodamo licenco](https://reuse.software/)
- [Priporočila za stil Octave](https://wiki.octave.org/Octave_style_guide)
- [Priporočila za stil Julia](https://docs.julialang.org/en/stable/manual/style-guide.html)
- [Sodelovalni urejevalnik za  Markdown](https://hackmd.io)